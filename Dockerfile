FROM ubuntu:18.04
RUN apt-get update && \
    apt-get install -y openssh-server net-tools vim iputils-ping dnsutils curl iptables
ADD authorized_keys /root/.ssh/
ENTRYPOINT bash -xc "/etc/init.d/ssh start; tail -f /dev/null"
